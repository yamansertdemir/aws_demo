from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os

application = Flask(__name__)

#Imports DB Config from OS Environment Variables

MYSQLDB_USER = os.environ["MYSQLDB_USER"]
MYSQLDB_PW = os.environ["MYSQLDB_PW"]
MYSQLDB_DBURL = os.environ["MYSQLDB_DBURL"]
MYSQLDB_DB = os.environ["MYSQLDB_DB"]

#SQLALCHEMY DB Conf
DB_URL = 'mysql://{user}:{pw}@{url}/{db}'.format(user=MYSQLDB_USER,pw=MYSQLDB_PW,url=MYSQLDB_DBURL,db=MYSQLDB_DB)

application.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
application.config['SQLALCHEMY_TRACK_MODIFICATIONS']= False
db = SQLAlchemy(application)

#DB Session Function
def create_session(config):
    engine = create_engine(config['SQLALCHEMY_DATABASE_URI'])
    Session = sessionmaker(bind=engine)
    session = Session()
    session._model_changes = {}
    return session

#Import routes
from apiapp import routes
