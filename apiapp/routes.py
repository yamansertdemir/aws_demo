from flask import request, jsonify
from apiapp import application, create_session
from apiapp.models import UserTable
from datetime import datetime

#Created DB Session
dbsession = create_session(application.config)

#Home Page 
@application.route('/')
def index():
    return 'Hello to API Demo App!'

#Returns Birthday
@application.route('/hello/<string:user_name>', methods=["GET"])
def get_user(user_name):
    def calculate_dates(original_date, thisday):
        delta1 = datetime(thisday.year, original_date.month, original_date.day)
        delta1date = datetime.date(delta1)
        delta2 = datetime(thisday.year+1, original_date.month, original_date.day)
        delta2date = datetime.date(delta2)
        if (delta1date-thisday).days >= 0:
           days = (delta1date-thisday).days
        else:
           days = (delta2date-thisday).days
        return days

    birthday = dbsession.query(UserTable.date_of_birth).filter(UserTable.first_name.like(user_name)).first().date_of_birth
    #print(birthday)  #Check if it is correct
    now=datetime.now()
    today = datetime.date(now)
    diffday = calculate_dates(birthday, today)

    if diffday <= 5 and diffday > 0:
        return jsonify('message','Hello, '+user_name+' Your birthday is in 5 days'), 200
    if diffday == 0:
        return jsonify('message','Hello, '+user_name+' Happy birthday!'), 200
    else:
        return 'Your birthday is out of range', 404

#Creates new user
@application.route('/hello/<string:user_name>', methods=["PUT"])
def create_user(user_name):
    dict_body = request.get_json() #convert body to dictionary
    #print(dict_body) #have a look at what is coming in
    userparams= UserTable(first_name=user_name,
                          date_of_birth=dict_body['dateOfBirth'])
    dbsession.add(userparams)
    dbsession.commit()
    return '', 204

