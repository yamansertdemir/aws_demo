from apiapp import db

class UserTable():
    def __init__(self):
        self.id = 1
        self.first_name = ""
        self.date_of_birth = date.today()

class UserTable(db.Model):
    __tablename__ = 'UserTable'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(120), unique=True, nullable=True)
    date_of_birth = db.Column(db.Date, unique=False, nullable=True)

    def __repr__(self):
        return '<UserTable %r>' % self.first_name

